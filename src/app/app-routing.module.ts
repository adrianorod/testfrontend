import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransacoesComponent } from './components/transacoes/transacoes.component';
import { BeneficiosComponent } from './components/beneficios/beneficios.component';
import { HistoricoComponent } from './components/historico/historico.component';

const routes: Routes = [
  { path: 'transacoes', component: TransacoesComponent },
  { path: 'beneficios', component: BeneficiosComponent },
  { path: 'historico', component: HistoricoComponent },
  { path: '', redirectTo: 'transacoes', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
