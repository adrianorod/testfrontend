export default class Utils {

  public static getValorMoeda(value: any) {
    if(typeof value === 'number') {
      return value.toLocaleString('pt-BR', { minimumFractionDigits: 2 });
    } else {
      return value;
    }
  }
}