import { Injectable, EventEmitter, Output } from '@angular/core';
import { ClientModel } from '../models/client.model';
import { Observable, of } from 'rxjs';
import { CLIENTS } from '../data/mock-clients';
import { CLIENTDETAILS } from '../data/mock-clientdetails';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  activeClient!: ClientModel;
  activeClientUpdated: EventEmitter<any> = new EventEmitter();

  constructor() { }

  getClients(): Observable<ClientModel[]> {
    return of(CLIENTS);
  }

  getClientDetails(): Observable<Object[]> {
    return of(CLIENTDETAILS);
  }

  setActiveClient(client: ClientModel) {
    this.activeClient = client;
    this.activeClientUpdated.emit(this.activeClient);
  }

  deleteActiveClient() {
    delete this.activeClient;
    this.activeClientUpdated.emit();
  }
  
  getActiveClient() {
    return this.activeClientUpdated;
  }
}
