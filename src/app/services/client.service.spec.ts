import { ClientService } from './client.service';
import { CLIENTS } from '../data/mock-clients';
import { CLIENTDETAILS } from '../data/mock-clientdetails';

describe('Suíte de testes de ClientService', () => {
  let service;

	beforeEach(() => {
    service = new ClientService();
  });

  it('Deve retornar lista de clientes', () => {
    service.getClients().subscribe(res => {
      expect(res).toEqual(CLIENTS);
    });
  });

  it('Deve retornar os detalhes dos clientes', () => {
    service.getClientDetails().subscribe(res => {
      expect(res).toEqual(CLIENTDETAILS);
    });
  });

  it('Deve atribuir cliente ativo', () => {
    service.setActiveClient(CLIENTS[0]);
    expect(service.activeClient).toEqual(CLIENTS[0]);
  });

  it('Deve excluir cliente atribuído', () => {
    service.setActiveClient(CLIENTS[0]);
    service.deleteActiveClient();
    expect(service.activeClient).toEqual(undefined);
  });

  it('Deve emitir evento quando propriedade activeClient for alterada', () => {
    service.getActiveClient().subscribe((res) => {
      expect(res).toEqual(undefined);
    });
  });
});
