import { Injectable } from '@angular/core';
import { PONTOS } from '../data/mock-pontos';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotaModel } from '../models/nota.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotaService {

  constructor(private http: HttpClient) { }

  getPontos(): Observable<any[]> {
    return of(PONTOS);
  }

  cadastraNotas(notas: NotaModel[]): Observable<NotaModel[]> {
    console.log('cadastra notas service')
    const url = '';
    return this.http.post<NotaModel[]>(url, notas)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Ocorreu um erro:', error.error.message);
    } else {
      console.error(
        `Backend retornou o código ${error.status}, ` +
        `Body: ${error.error}`);
    }
    return throwError(
      'Algo deu errado, tente novamente mais tarde.');
  };

}
