import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from './services/client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ ClientService ]
})
export class AppComponent {
  activeRoute: string = '';

  constructor(private route: Router) {
    this.activeRoute = route.url;
  }

  ngOnInit() {
  }
}
