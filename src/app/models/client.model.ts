import { NotaModel } from './nota.model';

export interface ClientModel {
  codigo: number;
  nome: string;
  email: string;
  cpf: string;
  notas: NotaModel[];
}