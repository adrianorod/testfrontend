export interface NotaModel {
  codigoCliente: number;
  loja: string;
  numero: number;
  data: Date;
  cadastro: Date;
  formaPagamento: string;
  valor: number;
}