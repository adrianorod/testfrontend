import { Component, OnInit } from '@angular/core';
import { ClientModel } from '../../models/client.model';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  clientList: ClientModel[] = [];
  clientListFiltered: ClientModel[] = [];
  clientDetailsList!: any[];
  messages: any[] = [];
  busca: string = '';

  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.getClients();
    this.getClientDetailsList();
  }

  getClients() {
    this.clientService.getClients()
      .subscribe(data => {
        this.clientList = data;
        this.clientListFiltered = this.clientList;
      });
  }

  getClientDetailsList() {
    this.clientService.getClientDetails()
      .subscribe(data => {
        this.clientDetailsList = data;
      })
  }

  filterClient() {
    this.clientListFiltered = Object.assign([], this.clientList).filter(
       (item: ClientModel) => item.nome.toLowerCase().indexOf(this.busca.toLowerCase()) > -1
    );
  }

  selectClient(codigo: any) {
    let clientClicked;

    this.clientDetailsList.forEach(client => {
      if(client.codigo === codigo) clientClicked = client;
    });

    clientClicked ? this.clientService.setActiveClient(clientClicked) : this.showMessage('Os dados desse cliente não foram encontrados.');
  }

  unselectClient() {
    this.clientService.deleteActiveClient();
  }

  showMessage(text: string) {
    this.messages.push(text);
    setTimeout(() => {
      this.messages = [];
    }, 2000);
  }
}
