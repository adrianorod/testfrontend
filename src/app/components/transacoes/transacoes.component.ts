import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { NotaService } from '../../services/nota.service';
import { NotaModel } from '../../models/nota.model';
import { NgForm } from '@angular/forms';
import Utils from '../../utils/utils';

@Component({
  selector: 'app-transacoes',
  templateUrl: './transacoes.component.html',
  styleUrls: ['./transacoes.component.scss']
})
export class TransacoesComponent implements OnInit {
  activeClient!: any;
  generalPontos!: any[];
  clientPontos = {
    saldo: 0,
    total: 0,
    utilizados: 0,
    expirados: 0
  };
  notas: NotaModel[] = [];
  exibeBotaoExcluirProp: number = -1;


  constructor(private clientService: ClientService, private notaService: NotaService) { }

  ngOnInit() {
    this.getGeneralPontos();
    this.checkClient();
  }

  getGeneralPontos() {
    this.notaService.getPontos()
      .subscribe(data => {
        this.generalPontos = data;
      });
  }

  getClientPontos(activeClient: any) {
    if(activeClient) {
      this.generalPontos.forEach(pontos => {
        pontos.saldo = 0;
        if(pontos.codigoCliente === activeClient.codigo) this.clientPontos = pontos;
      });
    } else {
      this.clientPontos = {
        saldo: 0,
        total: 0,
        utilizados: 0,
        expirados: 0
      }
      this.limpaNotas();
      this.exibeBotaoExcluirProp = -1;
    }
  }

  atualizaPontos(nota: NotaModel) {
    this.clientPontos.saldo += nota.valor;
  }

  checkClient() {
    if(this.clientService.activeClient) {
      this.activeClient = this.clientService.activeClient;
      this.getClientPontos(this.clientService.activeClient);
    }
    this.clientService.getActiveClient().subscribe(
      (activeClient) => {
        this.getClientPontos(activeClient);
        this.activeClient = activeClient;
      }
    );
  }

  adicionaNota(form: NgForm) {
    let notaFinal;

    if(this.activeClient) {
      notaFinal = form.value;
      notaFinal['codigoCliente'] = this.activeClient.codigo;
      notaFinal['cadastro'] = new Date();
      this.notas.push(notaFinal);
      this.atualizaPontos(notaFinal);
      form.reset();
    }
  }

  limpaCamposNota(event: Event, form: NgForm) {
    event.preventDefault();
    form.reset();
  }
  
  getValorMoeda(value: any) {
    return Utils.getValorMoeda(value);
  }

  cadastraNotas() {
    this.notaService.cadastraNotas(this.notas)
      .subscribe(results => {
        console.log('Notas cadastradas com sucessso ', results);
        this.limpaNotas();
      });
  }

  limpaNotas() {
    this.notas = [];
    this.clientPontos.saldo = 0;
  }

  exibeBotaoExcluir(index: number) {
    if(this.exibeBotaoExcluirProp !== index) {
      this.exibeBotaoExcluirProp = index;
    } else {
      this.exibeBotaoExcluirProp = -1;
    }
  }

  excluirNota(resNota: NotaModel) {
      this.clientPontos.saldo -= resNota.valor;
      this.exibeBotaoExcluirProp = -1;
      this.notas = this.notas.filter(function(nota){
          return nota != resNota;
      });
  }
}
