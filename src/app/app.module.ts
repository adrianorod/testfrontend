import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientService } from './services/client.service';
import { NotaService } from './services/nota.service';
import { TransacoesComponent } from './components/transacoes/transacoes.component';
import { BeneficiosComponent } from './components/beneficios/beneficios.component';
import { HistoricoComponent } from './components/historico/historico.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import {LOCALE_ID} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
registerLocaleData(localePt);
@NgModule({
  declarations: [
    AppComponent,
    ClientListComponent,
    TransacoesComponent,
    BeneficiosComponent,
    HistoricoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    CurrencyMaskModule
  ],
  providers: [
    ClientService,
    NotaService,
    { provide: LOCALE_ID, useValue: 'pt' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
