# TesteFrontEnd - Adriano Rodrigues

O projeto foi realizado seguindo as instruções do arquivo `instrucoes.md`, utilizando Angular 7.1.1 (automação por Angular CLI), SASS e Typescript.

O projeto utiliza components, rotas, services, mocks, models e utils. Cada um em sua devida pasta, dentro do caminho `src/app`.

## Baixando dependências

Faz-se necessário baixar as dependências do projeto antes de iniciá-lo. Para isso, execute o comando `npm install` através do terminal na pasta raiz do projeto.

## Iniciando projeto

Execute o comando `ng serve` através do terminal para iniciar o projeto e abra o link `http://localhost:4200/` em seu navegador. O app inicia na página apresentada pelo layout.

## Cobertura de testes

Apenas a service `client.service` está coberta por testes. Para rodá-los, basta executar o comando `ng test` através do terminal.